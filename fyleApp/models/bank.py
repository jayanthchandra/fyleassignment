
# Imports
##########

from . import db
from .base import Base

###############################################################################


# CBC Class
#############

class Bank(Base):
    __tablename__ = 'Bank'
    IFSC = db.Column(db.String(20), primary_key = True)
    BANK_ID = db.Column(db.String(50))
    BRANCH = db.Column(db.String(255))
    ADDRESS = db.Column(db.Text)
    CITY = db.Column(db.String(255))
    DISTRICT = db.Column(db.String(255))
    STATE = db.Column(db.String(255))
    BANK_NAME = db.Column(db.String(255))

    def __init__(self, ifsc, bank_id, branch, address, city, district, state, bank_name):
        self.IFSC = ifsc
        self.BANK_ID = bank_id
        self.BRANCH = branch
        self.ADDRESS = address
        self.CITY = city
        self.DISTRICT = district
        self.STATE = state
        self.BANK_NAME = bank_name

###############################################################################
