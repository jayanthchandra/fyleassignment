
# Imports
##########

from flask import current_app, jsonify
from . import api_routes
from fyleApp.models.bank import Bank

###############################################################################

# Routes
#########

@api_routes.route('/ifsc/<ifscId>',methods=['GET'])
def get_bankDetails(ifscId):
	current_app.logger.info('/api/v1/ifsc')
	bank = Bank.query.filter_by(IFSC=ifscId).first()
	if bank:
		return jsonify({'bank': Bank.serialize(bank)})
	else:
		return jsonify({'message': 'No Bank Found'})

@api_routes.route('/bank/<name>/<city>',methods=['GET'])
def get_bankBranches(name, city):
	current_app.logger.info('/api/v1/bank')
	banks = Bank.query.filter_by(BANK_NAME=name).filter_by(CITY=city).all()
	if banks:
		return jsonify({'bank': [Bank.serialize(bank) for bank in banks]})
	else:
		return jsonify({'message' : 'No Branches Found'})

###############################################################################
