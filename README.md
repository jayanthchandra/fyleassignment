# Fyle Assignment

1. Creating Virtual Environment
 `virtualenv venv -vvv`

2. Activate the Virtual Environment
  `source venv/bin/activate`

3. Install Requirement files
  `pip install -r requiremnets.txt`

4. For Running the application
   `python run.py`
   or 
   Using Gunicorn `gunicorn run:app`

## API DOCS

Uses API Versioning and REST concepts

### Problem Statement 
	Method: GET
- `/api/v1/ifsc/<ifsc_code>` -> Returns Bank if IFSC Code Exists
- `/api/v1/bank/<bank_name>/<city>` -> Returns Branches of all the Bank in that City 

### Utils
	Method: GET
- /ping -> To check if server is running

